package com.example.fedunalexander.servicejobapp

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.fedunalexander.servicejobapp.fragments.RequestFragment
import com.example.fedunalexander.servicejobapp.fragments.ServiceFragment
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionBarText = findViewById<TextView>(R.id.toolbar_text)
        bottomNavigationView = findViewById(R.id.bottom_nav)
        supportFragmentManager.beginTransaction().add(R.id.container, ServiceFragment()).commit()
        bottomNavigationView.setOnNavigationItemSelectedListener(
            BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.bottombaritem_service -> {
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.container, ServiceFragment(), "")
                            .commit()
                        actionBarText.text = "Сервисы"
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.bottombaritem_requset -> {
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.container, RequestFragment(), "")
                            .commit()
                        actionBarText.text = "Новая заявка"
                        return@OnNavigationItemSelectedListener true
                    }
                }
                false
            })
    }
}