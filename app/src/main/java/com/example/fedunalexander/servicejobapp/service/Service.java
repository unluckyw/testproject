package com.example.fedunalexander.servicejobapp.service;

public class Service{
    public String icon;

    public String title;

    public String link;

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public Service(){
    }
}