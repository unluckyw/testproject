package com.example.fedunalexander.servicejobapp.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.example.fedunalexander.servicejobapp.AnswerApi
import com.example.fedunalexander.servicejobapp.R
import com.example.fedunalexander.servicejobapp.service.ServerAnswer
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException

class RequestFragment : Fragment() {

    internal lateinit var ImageButt: ImageButton
    internal lateinit var RequestName: TextView
    internal lateinit var RequestComment: TextView
    internal lateinit var view: View

    internal var bitmap: Bitmap? = null

    internal var photoUri: Uri? = null
    internal lateinit var picFile: File
    internal lateinit var CurrentPhotoPath: String
    internal val CAMERA_REQUEST = 1888

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view = inflater.inflate(R.layout.request_layout, container, false)
        RequestName = view.findViewById(R.id.tv_request_name)
        RequestComment = view.findViewById(R.id.tv_request_comment)

        setHasOptionsMenu(true)

        ImageButt = view.findViewById(R.id.ib_photo_camera)
        ImageButt.setOnClickListener { takePhoto() }
        return view
    }

    private fun takePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val context = context
        val imagePath = context!!.getExternalFilesDir(null)
        picFile = File(imagePath, "image.png")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            photoUri = FileProvider.getUriForFile(
                context,
                String.format("%s.provider", context.applicationContext.packageName),
                picFile
            )
        } else {
            photoUri = Uri.fromFile(picFile)
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
        intent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        CurrentPhotoPath = picFile.absolutePath
        startActivityForResult(intent, CAMERA_REQUEST)
    }

    private fun uploadRequest() {
        val namePart = RequestBody.create(MultipartBody.FORM, RequestName.text.toString())
        val commentPart = RequestBody.create(MultipartBody.FORM, RequestComment.text.toString())
        var photoPart: MultipartBody.Part? = null
        if (photoUri != null) {
            val photo = RequestBody.create(
                MediaType.parse(context!!.contentResolver.getType(photoUri!!)!!),
                picFile
            )
            photoPart = MultipartBody.Part.createFormData("photo", picFile.name, photo)
        }

        val builder = Retrofit.Builder()
            .baseUrl("http://job.softinvent.ru")
            .addConverterFactory(GsonConverterFactory.create())

        val retrofit = builder.build()

        val answerApi = retrofit.create(AnswerApi::class.java)

        val call = answerApi.uploadRequest(namePart, commentPart, photoPart!!)
        call.enqueue(object : Callback<ServerAnswer> {
            override fun onResponse(call: Call<ServerAnswer>, response: Response<ServerAnswer>) {
                var filename: String?
                try {
                    filename = response.body()!!.filename
                } catch (e: Exception) {
                    filename = null
                }

                if (filename != null) {
                    RequestName.text = ""
                    RequestComment.text = ""
                    ImageButt.setImageResource(R.drawable.ic_camera)
                } else if (response.errorBody() != null) {
                    var errorString: String? = null
                    try {
                        errorString = response.errorBody()!!.string()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    val gson = Gson()
                    val serverAnswer = gson.fromJson(errorString, ServerAnswer::class.java)
                    val dialog = ErrorDialog()
                    val argString = Bundle()
                    argString.putCharSequence("error", serverAnswer.error)
                    dialog.arguments = argString
                    dialog.show(fragmentManager!!, "dialog")
                }
            }

            override fun onFailure(call: Call<ServerAnswer>, t: Throwable) {
                Toast.makeText(context, "Ошибка отправки запроса: $t", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            val targetW = ImageButt.width
            val targetH = ImageButt.height

            // получаем размеры изображения
            val bmOptions = BitmapFactory.Options()
            bmOptions.inJustDecodeBounds = true
            BitmapFactory.decodeFile(CurrentPhotoPath, bmOptions)
            val photoW = bmOptions.outWidth
            val photoH = bmOptions.outHeight

            // определяем коэффициент масштабирования
            val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

            // преобразуем фотографию в Bitmap объект соответствующий View
            bmOptions.inJustDecodeBounds = false
            bmOptions.inSampleSize = scaleFactor
            bmOptions.inPurgeable = true

            bitmap = BitmapFactory.decodeFile(CurrentPhotoPath, bmOptions)
            ImageButt.setImageBitmap(bitmap)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val name = RequestName.text.toString().trim { it <= ' ' }
        val comment = RequestComment.text.toString().trim { it <= ' ' }
        if (name.isEmpty()) {
            RequestName.error = "Имя обязательно"
            RequestName.requestFocus()
            return false
        }
        if (comment.isEmpty()) {
            RequestComment.error = "Описание обязательно"
            RequestComment.requestFocus()
            return false
        }
        uploadRequest()
        return super.onOptionsItemSelected(item)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            RequestName.text = savedInstanceState.getCharSequence("name")
            RequestComment.text = savedInstanceState.getCharSequence("comment")
            bitmap = savedInstanceState.getParcelable("image")
            ImageButt.setImageBitmap(bitmap)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putCharSequence("name", RequestName.text)
        outState.putCharSequence("comment", RequestComment.text)
        outState.putParcelable("image", bitmap)
        super.onSaveInstanceState(outState)
    }
}
