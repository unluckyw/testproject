package com.example.fedunalexander.servicejobapp

import com.example.fedunalexander.servicejobapp.service.ServerAnswer
import com.example.fedunalexander.servicejobapp.service.Service

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface AnswerApi {

    @get:GET("/services.php?os=android")
    val services: Call<List<Service>>

    @Multipart
    @POST("/upload.php")
    fun uploadRequest(
        @Part("name") name: RequestBody,
        @Part("comment") comment: RequestBody,
        @Part photo: MultipartBody.Part
    ): Call<ServerAnswer>
}
