package com.example.fedunalexander.servicejobapp.service;

public class ServerAnswer {

    String error;
    String filename;

    public String getFilename() {
        return filename;
    }

    public String getError() {
        return error;
    }
}
