package com.example.fedunalexander.servicejobapp.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class ErrorDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val error = arguments!!.getString("error")
        builder.setTitle("Ошибка отправки заявки")
            .setMessage(error)
            .setPositiveButton("OK") { dialog, _ -> dialog.cancel() }
        return builder.create()
    }
}
