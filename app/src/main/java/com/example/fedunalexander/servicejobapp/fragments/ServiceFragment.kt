package com.example.fedunalexander.servicejobapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fedunalexander.servicejobapp.AnswerApi
import com.example.fedunalexander.servicejobapp.R
import com.example.fedunalexander.servicejobapp.service.Service
import com.example.fedunalexander.servicejobapp.service.ServiceAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServiceFragment : Fragment() {

    internal var list: List<Service>? = null

    internal lateinit var serviceList: RecyclerView
    internal lateinit var serviceAdapter: ServiceAdapter

    internal lateinit var loadingService: ProgressBar
    internal lateinit var errorMessage: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.service_layout, container, false)

        val context = context

        serviceList = view.findViewById(R.id.rv_service_list)

        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        serviceList.layoutManager = linearLayoutManager
        serviceList.setHasFixedSize(true)
        serviceAdapter = ServiceAdapter()
        serviceList.adapter = serviceAdapter

        loadingService = view.findViewById(R.id.pb_loading_service)
        errorMessage = view.findViewById(R.id.tv_error_message)
        loadingServiceData()
        return view
    }

    private fun showServiceData() {
        serviceList.visibility = View.VISIBLE
        errorMessage.visibility = View.INVISIBLE
    }

    private fun showErrorMessage() {
        serviceList.visibility = View.INVISIBLE
        errorMessage.visibility = View.VISIBLE
    }

    private fun loadingServiceData() {
        loadingService.visibility = View.VISIBLE
        showServiceData()
        loadServices()
    }

    private fun loadServices() {
        val builder = Retrofit.Builder()
            .baseUrl("http://job.softinvent.ru")
            .addConverterFactory(GsonConverterFactory.create())

        val retrofit = builder.build()

        val answerApi = retrofit.create(AnswerApi::class.java)

        val call = answerApi.services

        call.enqueue(object : Callback<List<Service>> {

            override fun onResponse(
                call: Call<List<Service>>,
                response: retrofit2.Response<List<Service>>
            ) {
                loadingService.visibility = View.INVISIBLE
                list = response.body()
                if (list != null) {
                    showServiceData()
                    serviceAdapter.setServiceData(list)
                } else {
                    showErrorMessage()
                }
            }

            override fun onFailure(call: Call<List<Service>>, t: Throwable) {
                showErrorMessage()
            }
        })
    }
}
