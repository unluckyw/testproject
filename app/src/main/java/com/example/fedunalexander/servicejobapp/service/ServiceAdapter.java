package com.example.fedunalexander.servicejobapp.service;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fedunalexander.servicejobapp.R;

import java.util.List;

final public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceAdapterViewHolder> {

    private List<Service> serviceList;

    public ServiceAdapter() {

    }

    @NonNull
    @Override
    public ServiceAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutIdForItemList = R.layout.list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForItemList, viewGroup, false);
        return new ServiceAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ServiceAdapterViewHolder serviceAdapterViewHolder, int i) {
        ImageView imageView = serviceAdapterViewHolder.mServiceIcon;
        TextView serviceTitle = serviceAdapterViewHolder.mServiceTitle;
        Context context = serviceAdapterViewHolder.itemView.getContext();
        final Service item = serviceList.get(i);
        Glide.with(context).load(item.icon).into(imageView);
        serviceTitle.setText(item.title);
        serviceAdapterViewHolder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, item.link);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        if (null == serviceList) return 0;
        return serviceList.size();
    }

    public void setServiceData(List<Service> services) {
        serviceList = services;
        notifyDataSetChanged();
    }

    public class ServiceAdapterViewHolder extends RecyclerView.ViewHolder {

        public ImageView mServiceIcon;

        public TextView mServiceTitle;

        public ServiceAdapterViewHolder(View view) {
            super(view);
            mServiceIcon = itemView.findViewById(R.id.iv_service_icon);
            mServiceTitle = itemView.findViewById(R.id.tv_service_title);
        }
    }
}
